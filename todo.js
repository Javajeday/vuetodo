const app = new Vue({
    el: '.wrapper',
    data: {
        name: '',
        description: '',
        priority: '',
        todos: [
            {
                name: 'Сделать Todo',
                description: 'Сделать Todo на VueJS и не наговнокодить',
                priority: 0,
                finished: false
            }
        ],
        updatedIndex: null
    },
    methods: {
        createTodo: function(event) {
            event.preventDefault();
            if (this.updatedIndex !== null) {
                this.todos[this.updatedIndex].name = this.name;
                this.todos[this.updatedIndex].description = this.description;
                this.todos[this.updatedIndex].priority = Number(this.priority);

                this.clearUserInputs();
            } else {
                this.todos.unshift({
                    name: this.name,
                    description: this.description,
                    priority: Number(this.priority),
                    finished: false
                })
            }

            this.updatedIndex = null;
            this.clearUserInputs();
            this.setStorageData();
        },
        updateTodo: function(index) {
            this.updatedIndex = index;
            this.name = this.todos[index].name;
            this.description = this.todos[index].description;
            this.priority = this.todos[index].priority;
        },
        deleteTodo: function(index) {
            this.todos.splice(index, 1);
        },
        finishTodo: function(index) {
            this.todos[index].finished = !this.todos[index].finished;
            this.setStorageData();
        },
        clearUserInputs: function() {
            this.description = this.name = '';
            this.priority = undefined;
        },
        getStorageData() {
            if (localStorage.getItem('todos')) {
                this.todos = JSON.parse(localStorage.getItem('todos'));
            } else {
                localStorage.setItem('todos', JSON.stringify([]));
            }
        },
        setStorageData() {
            localStorage.setItem('todos', JSON.stringify(this.todos))
        }
    },
    mounted: function() {
        this.getStorageData();
    }
})

console.log(app);